import { sum } from '../sum';

test('suma dos números y te da el resultado', () => {
    expect(sum(1, 2)).toBe(3);
    expect(sum(2, 2)).toBe(4);
    expect(sum(3, 2)).toBe(5);
    expect(sum("a", "b")).toBe("ab");
});