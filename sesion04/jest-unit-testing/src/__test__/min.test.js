import { min } from '../min';

test('adds 1 - 2 to equal 3', () => {
    expect(min(2, 1)).toBe(1);
    expect(min(4, 2)).toBe(2);
    expect(min(6, 2)).toBe(4);
});